# code for simulations and case study in Paciorek et al. manuscript on statistical methods for event attribution

# prepared June 2017, last updated December 2017

# Software dependencies:
# R package climextRemes version 0.2
# R package ncdf4 version 1.16
# R package ggplot2 version 2.2.1
# R package boot version 1.3.19

# Software environment  
# Rscript -e "sessionInfo()"

# R version 3.4.3 (2017-11-30)
# Platform: x86_64-pc-linux-gnu (64-bit)
# Running under: Ubuntu 16.04.2 LTS

# locale:
#  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
#  [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
#  [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
#  [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
#  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
# [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       

# attached base packages:
# [1] stats     graphics  grDevices utils     datasets  methods   base     

### 1: simulation study for manuscript

# Wang-Shan confidence intervals are computationally intensive but
# can be precomputed for various binomial sample sizes and numbers of
# events. This step precomputes these confidence intervals use
# code provided by Weizhen Wang
# This produces wangCI_tables_{90,95,975}.Rda
R CMD BATCH --no-save precalculate_wang_cis.R

# run simulations
R CMD BATCH --no-save sim_study.R  # uses wang.R, wilson.R

# simulation results plots for manuscript
Rscript -e "nVal <- 50; source('plot.R')"  # not used for revision
Rscript -e "nVal <- 100; source('plot.R')"
Rscript -e "nVal <- 400; source('plot.R')"

### 2: case study for manuscript

R CMD BATCH --no-save example.R
