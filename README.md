This repository provides the complete code and data for Paciorek, Stone, and Wehner paper ([arXiv version](http://arxiv.org/abs/1706.03388)) on uncertainty quantification for event attribution analyses. The focus is on sampling uncertainty using standard frequentist statistics confidence interval methods.

The full workflow is documented in master.sh.